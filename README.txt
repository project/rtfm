CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The RTFM module allows other module maintainers to create documentation pages
for their modules.

After installing the module, you will see a Documentation link in the toolbar.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/rtfm

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/rtfm

REQUIREMENTS
------------

This module requires the following external JavaScript libraries:

 * Marked (https://github.com/markedjs/marked)
 * highlight.js (https://highlightjs.org)

Note: they are already included as part of the module.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * Configure the user permissions in Administration » People » Permissions:

   - Access RTFM documentation

     This will give the users permission to view documentation pages.

MAINTAINERS
-----------

Current maintainers:
 * Emerson Jair (dungahk) - https://www.drupal.org/u/dungahk
