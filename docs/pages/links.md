## How to add links

Links follow the Markdown language specification, check
[this page](https://www.markdownguide.org/basic-syntax/#links).

If you want to link to your own documentation pages,
you can use the following structure:

```markdown
[Link text](/pages/file_name.md)
```

A link to this page looks like the following:

```markdown
[Link text](/pages/links.md)
```

The `.md` is optional.

To link to the homepage of your documentation (the README.md file),
use the following:

```markdown
[Home](/)
```

Change the `Home` text as you wish.

Learn how to add components to your documentation on
[this page](/pages/components).
