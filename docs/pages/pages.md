## How to add pages

Just like the README.md, you will need to create a directory called `pages`
inside the `docs` directory in order to use pages.

After you create the directory, you can create how many markdown files as you
want in the directory.

See this example of the module structure:

```plaintext
module_name/ 📂
  module_name.info.yml
  module_name.module
  docs/ 📂
    README.md
    pages/ 📂
      how.md
      pages.md
```

Check [this page](/pages/links.md) to understand how to add links to your
documentation.
