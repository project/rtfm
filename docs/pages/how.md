## How to create a documentation for your module

RTFM is very easy to add to your module. The only thing you have to do is to
create a directory named `docs` at the root of your module and create a
README.md file. This file will act as the homepage of your documentation.

You can check the RTFM module for an example. This is how the module structure
looks like:

```plaintext
module_name/ 📂
  module_name.info.yml
  module_name.module
  docs/ 📂
    README.md
```

This is the bare minimum your module needs to be displayed in the list of
documentation.

See how to create pages on [this page](/pages/pages.md).
