## Permission

The RTFM module defines the **Access RTFM documentation**
(*access rtfm documentation*) permission.

If you want to allow users to access the documentation,
make sure the user has a role with that permission.

## WARNING
Bear in mind that users with permission to access the documentation
will be able to access any documentation available.

With that being said, we strongly suggest that you give this permission
only to roles you trust.
In fact, this module audience is primarily site administrators.
