## Sidebar

The sidebar component will display a sidebar navigation in your documentation.

In order to add a sidebar to your documentation, you will need to create a new
directory called `components` inside the `docs` directory.

Then, create a file called `sidebar.md`. The file name is important and MUST
not be changed.

Example of a module structure:

```plaintext
module_name/ 📂
  module_name.info.yml
  module_name.module
  docs/ 📂
  |  README.md
  |  pages/ 📂
  |  |  how.md
  |  |  pages.md
  |  components/ 📂
  |  |  sidebar.md
```
