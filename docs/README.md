## RTFM Documentation

RTFM is a module that allows other modules to specify documentation for the
site administrators.

RTFM relies on the [Markdown language](https://www.markdownguide.org).
It uses [Marked](https://github.com/markedjs/marked) to render HTML code
and [highlight.js](https://highlightjs.org) for
[syntax highlighting](https://www.markdownguide.org/extended-syntax/#syntax-highlighting).

Learn how to create a documentation for your module [here](/pages/how.md).

Learn how to give people access to the documentation
[here](/pages/permission.md).
