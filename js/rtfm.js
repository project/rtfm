(($, Drupal) => {
  /**
   * Initialize.
   *
   * @type {{}}
   */
  Drupal.behaviors.Rtfm = {
    /**
     * Renders the readme by default.
     *
     * @param {object} context
     *   Drupal context.
     * @param {object} settings
     *   Drupal settings.
     * @param {object} settings.rtfm
     *   The RTFM data.
     * @param {string} settings.rtfm.readme
     *   The README.md content.
     * @param {object} settings.rtfm.pages
     *   The documentation pages.
     * @param {object} settings.rtfm.components
     *   The documentation components.
     * @param {string} settings.rtfm.components.sidebar
     *   The sidebar component.
     */
    attach: (context, settings) => {
      if (Drupal.behaviors.Rtfm.rtfm) {
        return;
      }
      Drupal.behaviors.Rtfm.rtfm = settings.rtfm;
      Drupal.behaviors.Rtfm.renderMarkdown(settings.rtfm.readme, ".rtfm-page");
      Drupal.behaviors.Rtfm.renderMarkdown(
        settings.rtfm.components.sidebar,
        ".rtfm-component"
      );
      Drupal.behaviors.Rtfm.activeSidebarItem();

      if (window.location.hash) {
        Drupal.behaviors.Rtfm.navigate(window.location.hash);
      }

      Drupal.behaviors.Rtfm.initSearch();
    },

    /**
     * Returns HTML code from markdown.
     *
     * @param {string} markdown
     *   The markdown text.
     *
     * @return {string}
     *   The HTML code based on the markdown.
     */
    render: markdown => {
      window.marked.use({ renderer: Drupal.behaviors.Rtfm.renderer });
      return window.marked(markdown);
    },

    /**
     * Renders a documentation page.
     *
     * @param {string} markdown
     *   The markdown text.
     * @param {string} context
     *   The context in which it should be rendered to. (A CSS selector).
     */
    renderMarkdown: (markdown, context) => {
      const rendered = Drupal.behaviors.Rtfm.render(markdown);
      $(context).html(rendered);
      Drupal.behaviors.Rtfm.listenClick(context);
      $("pre code", context).each((_, element) => {
        window.hljs.highlightBlock(element);
      });
    },

    /**
     * Listen to the click event for page links.
     *
     * @param {string} context
     *   The context in which the event should attach to. (A CSS selector).
     */
    listenClick: context => {
      $(".rtfm-link", context).click(event => {
        Drupal.behaviors.Rtfm.click(event);
      });
    },

    /**
     * Updated the current active sidebar item/link.
     *
     * @param {string} page
     *   The page that was clicked/navigated to.
     */
    activeSidebarItem: (page = "/") => {
      const pageName = page.replace(".md", "");
      const $sidebarLinks = $(".rtfm-component a");
      $sidebarLinks.removeClass("rtfm-active");
      $sidebarLinks
        .filter((index, element) => $(element).attr("href") === pageName)
        .addClass("rtfm-active");
    },

    /**
     * Checks whether the URL is internal.
     *
     * @param {string} url
     *   The URL.
     *
     * @return {boolean}
     *   TRUE if the link is internal, FALSE otherwise.
     */
    isLinkInternal(url) {
      return url.charAt(0) === "/" && url.charAt(1) !== "/";
    },

    /**
     * Override link rendering from marked.
     *
     * @param {string} href
     *   The link href property.
     * @param {string} text
     *   The link text.
     * @return {string}
     *   The HTML that should be used instead of the default.
     *
     * @see Drupal.behaviors.Rtfm.renderer
     * @see Drupal.behaviors.Rtfm.renderPage
     * @link https://marked.js.org/#/USING_PRO.md
     */
    link(href, text) {
      let className = 'class="rtfm-link-external"';
      if (Drupal.behaviors.Rtfm.isLinkInternal(href)) {
        className = 'class="rtfm-link"';
      }
      return `<a ${className} href="${href}" title="${text}">${text}</a>`;
    },

    /**
     * Callback for click event on a link.
     *
     * @param {MouseEvent} event
     *   The click event.
     * @param {string} context
     *   The context in which the click event happened. (A CSS selector).
     *
     * @see Drupal.behaviors.Rtfm.link
     */
    click(event, context = ".rtfm-page") {
      event.preventDefault();
      event.stopPropagation();
      const href = $(event.target).attr("href");
      Drupal.behaviors.Rtfm.navigate(href, context);
    },

    /**
     * Navigates to a documentation page.
     *
     * @param {string} hrefOriginal
     *   The original href. (It will be parsed in the function).
     * @param {string} context
     *   The context in which the navigation is happening. (A CSS selector).
     *
     * @see Drupal.behavior.Rtfm.click
     */
    navigate(hrefOriginal, context = ".rtfm-page") {
      const href = hrefOriginal.replace("#", "");
      window.location.hash = href;
      const paths = href.split("/").filter(Boolean);
      if (paths.length === 0) {
        Drupal.behaviors.Rtfm.activeSidebarItem();
        Drupal.behaviors.Rtfm.renderMarkdown(
          Drupal.behaviors.Rtfm.rtfm.readme,
          context
        );

        return;
      }
      const markdown = paths.reduce((result, path) => {
        const realPath = path.replace(".md", "");

        return result[realPath];
      }, Drupal.behaviors.Rtfm.rtfm);
      Drupal.behaviors.Rtfm.activeSidebarItem(href);
      Drupal.behaviors.Rtfm.renderMarkdown(markdown, context);
    },

    /**
     * Initialize the search functionality.
     */
    initSearch() {
      const allPages = [
        {
          name: "readme",
          value: Drupal.behaviors.Rtfm.rtfm.readme
        }
      ];
      const { pages } = Drupal.behaviors.Rtfm.rtfm;
      const otherPages = Object.keys(pages).map(page => ({
        name: page,
        value: pages[page]
      }));
      allPages.push(...otherPages);
      const $searchInput = $(".rtfm-search input");
      $searchInput.on(
        "input",
        Drupal.debounce(() => {
          const inputValue = $searchInput.val();
          if (!inputValue) {
            $(".rtfm-search-results").html("");
            return;
          }

          const search = inputValue.toLowerCase();
          const matches = allPages
            .filter(page => page.value.toLowerCase().includes(search))
            .map(match => {
              const href =
                match.name === "readme" ? "/" : `/pages/${match.name}`;
              const text = $(`.rtfm-component a[href="${href}"]`).text();
              return Drupal.behaviors.Rtfm.link(href, text);
            });
          $(".rtfm-search-results").html(`
          <ul>
            ${matches.map(match => `<li>${match}</li>`).join(" ")}
          </ul>
        `);
          Drupal.behaviors.Rtfm.listenClick(".rtfm-search-results");
        }, 250)
      );
    },

    /**
     * Contains the whole markdown structure.
     */
    rtfm: null,

    /**
     * Contains the current markdown page being displayed.
     */
    current: null,

    /**
     * Renderer object to override default ones from Marked.
     */
    renderer: {
      /**
       * Overrides the default link renderer from Marked.
       *
       * @param {string} href
       *   The link href attribute.
       * @param {string} title
       *   The link title.
       * @param {string} text
       *   The link text.
       *
       * @return {string}
       *   The HTML that should be used instead of the default.
       */
      link(href, title, text) {
        return Drupal.behaviors.Rtfm.link(href, text);
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
