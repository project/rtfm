<?php

namespace Drupal\rtfm\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RtfmController.
 *
 * @package Drupal\rtfm\Controller
 */
class RtfmController extends ControllerBase {

  /**
   * Module handler used to get the module information.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Used to get the documentation files information.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * RtfmController constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    FileSystemInterface $fileSystem
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @var \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler */
    $moduleHandler = $container->get('module_handler');

    /* @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = $container->get('file_system');

    return new static(
      $moduleHandler,
      $fileSystem
    );
  }

  /**
   * Display a list of modules that have documentation implemented.
   *
   * @return array
   *   The render array.
   */
  public function list(): array {
    $moduleDirectories = $this->moduleHandler->getModuleDirectories();
    $documentedModules = array_filter($moduleDirectories,
      function ($moduleDirectory) {
        $readmePath = "$moduleDirectory/docs/README.md";
        return file_exists($readmePath);
      }
    );

    $modules = [];
    foreach ($documentedModules as $module => $_) {
      $modules[$module] = Link::createFromRoute(
        $this->moduleHandler->getName($module),
        'rtfm.documentation',
        ['module' => $module]
      );
    }

    return [
      '#theme' => 'documentation_list',
      '#modules' => $modules,
    ];
  }

  /**
   * Builds the title for a module's documentation page.
   *
   * @param string $module
   *   The module machine name.
   *
   * @return string
   *   The title.
   */
  public function documentationTitle(string $module): string {
    $name = $this->moduleHandler->getName($module);

    return "Documentation for $name";
  }

  /**
   * Renders a module documentation.
   *
   * @param string $module
   *   The module name.
   *
   * @return array
   *   The render array.
   */
  public function documentation(string $module): array {
    if (!$this->moduleHandler->moduleExists($module)) {
      return [];
    }

    if (!$this->isModuleDocumented($module)) {
      return [];
    }

    $readme = file_get_contents($this->getReadmePath($module));

    $pagesPath = $this->getPagesPath($module);
    $pagesScanned = $this->fileSystem->scanDirectory($pagesPath, '/.*\.md/', ['key' => 'name']);
    $pages = [];
    foreach ($pagesScanned as $pageName => $pageInfo) {
      $pageContent = file_get_contents($pageInfo->uri);
      $pages[$pageName] = $pageContent;
    }

    $componentsPath = $this->getComponentsPath($module);
    $componentsScanned = $this->fileSystem->scanDirectory($componentsPath, '/.*\.md/', ['key' => 'name']);
    $components = [];
    foreach ($componentsScanned as $componentName => $componentInfo) {
      $pageContent = file_get_contents($componentInfo->uri);
      $components[$componentName] = $pageContent;
    }

    return [
      '#theme' => 'documentation',
      '#attached' => [
        'library' => [
          'rtfm/marked',
          'rtfm/highlight',
          'rtfm/rtfm',
        ],
        'drupalSettings' => [
          'rtfm' => [
            'readme' => $readme,
            'pages' => $pages,
            'components' => $components,
          ],
        ],
      ],
    ];
  }

  /**
   * Checks if a module has been documented.
   *
   * @param string $module
   *   The module machine name.
   *
   * @return bool
   *   TRUE if the module has documentation, FALSE otherwise.
   */
  protected function isModuleDocumented(string $module): bool {
    $dirs = $this->moduleHandler->getModuleDirectories();
    $moduleDirectory = $dirs[$module];
    $readmePath = "$moduleDirectory/docs/README.md";
    return file_exists($readmePath);
  }

  /**
   * Returns the path to a module.
   *
   * @param string $module
   *   The module machine name.
   *
   * @return string
   *   The path to the module.
   */
  protected function getModulePath(string $module): string {
    return $this->moduleHandler->getModuleDirectories()[$module];
  }

  /**
   * Returns the path to the README.md of a module.
   *
   * @param string $module
   *   The module machine name.
   *
   * @return string
   *   The README.md path.
   */
  protected function getReadmePath(string $module): string {
    $modulePath = $this->getModulePath($module);
    return "$modulePath/docs/README.md";
  }

  /**
   * Returns the path to the pages directory.
   *
   * @param string $module
   *   The module machine name.
   *
   * @return string
   *   The directory path.
   */
  protected function getPagesPath(string $module): string {
    $modulePath = $this->getModulePath($module);
    return "$modulePath/docs/pages";
  }

  /**
   * Returns the path to the components directory.
   *
   * @param string $module
   *   The module machine name.
   *
   * @return string
   *   The directory path.
   */
  protected function getComponentsPath(string $module): string {
    $modulePath = $this->getModulePath($module);
    return "$modulePath/docs/components";
  }

}
